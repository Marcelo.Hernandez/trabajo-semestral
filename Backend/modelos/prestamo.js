'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const PrestamoSchema = Schema(
    {
    libro: { type: Schema.ObjectId, ref: "libro" },     
    persona: { type: Schema.ObjectId, ref: "personas" },
    fecha : {type:String} //Cambiado a string ya que con type date y la fecha dd/mm/aa se pasaba de los caracteres con el tiempo que guardaba
    })

module.exports = mongoose.model('prestamo',PrestamoSchema)    